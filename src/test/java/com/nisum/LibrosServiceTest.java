package com.nisum;

import  org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.nisum.dao.LibrosDao;
import com.nisum.model.Libro;
import com.nisum.service.LibrosService;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
 

@RunWith(MockitoJUnitRunner.class)
public class LibrosServiceTest {
	
	//clase a la cual se realizará mock
	@Mock
	private LibrosDao librosDao;
	
	@InjectMocks
	private LibrosService librosService; 
	

	@Test
	public void deboCrearUnLibroYRetornarlo() {
		
		//arrange
		Libro libro = new Libro(); 
		Libro libroCreado = new Libro();
		
		//act
		when(librosDao.save(libro)).thenReturn(libro);
		libroCreado = librosService.crearLibro(libro);
		
		//assert
		Assert.assertNotNull(libroCreado);
		Assert.assertEquals(libro,libroCreado);
	}

	@Test
	public void ObtenerLibroPorId() {
		
		//arrange
		Libro libro = new Libro();
		//libro.setId(1);
		Libro libroRetornado = new Libro();
		
		//act
		when(librosDao.findOne(anyLong())).thenReturn(libro);
		libroRetornado = librosService.obtenerLibro(libro.getId());
		
		//assert
		Assert.assertNotNull(libroRetornado);
		Assert.assertEquals(libroRetornado, libro);
	}

	@Test
	public void ObtenerLibroPorTitulo() {
		
		//arrange
		Libro libro = new Libro();
		libro.setTitulo("Java");
		Libro libroRetornado = new Libro();
		
		//act
		when(librosDao.findByTitulo("Java")).thenReturn(libro);
		libroRetornado = librosService.obtenerLibro(libro.getTitulo());
		
		//assert 
		Assert.assertNotNull(libroRetornado);
		Assert.assertEquals(libro, libroRetornado);
	}

	@Test
	public void ObtenerTodosLosLibros() {
		
		//arrange
		List<Libro> mislibros = new ArrayList<Libro>();
		List<Libro> resultado = new ArrayList<Libro>();
		
		//act
		when(librosDao.findAll()).thenReturn(mislibros);
		resultado = librosService.obtenerTodos();
		
		//assert 
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultado, mislibros);
	}
	
}
